# mivid

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## TODO

Abhishek tasks:
----------------------
* upload & thumbnails

* stream

* Commenting

* Video gallery page


Amal tasks:
-----------------------
* search & search results page.

* User profile

* Notification system -> notify users on new comments on their videos and in replies to their comments -> me

* OAuth & User accounts (done)

* Handle creation (done)


# Release 1 (ToDOS):
- Video uploading.
- Get the initial video listing page up with paginations.
- Design the handle page
- Search results view with search working.
- Individual Video streaming page (gallery can be part of a later release)
- oAuth & user accounts.
- User profile page that lists all the user's uploads.
